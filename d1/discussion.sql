-- inserting records
INSERT INTO artists (name) VALUES("Riveramaya");
INSERT INTO artists (name) VALUES("Psy");

INSERT INTO albums(album_title, date_released, artist_id) VALUES(
    "Psy 6",
    "2012-1-1",
    3
);
INSERT INTO albums(album_title, date_released, artist_id) VALUES(
    "TRIP",
    "1996-1-1",
    1
);

INSERT INTO songs(song_name, length, genre, album_id) VALUES(
    "Gangnam Style",
    253,
    "KPOP",
    1
);
INSERT INTO songs(song_name, length, genre, album_id) VALUES(
    "Kundiman",
    234,
    "OPM",
    2
);
INSERT INTO songs(song_name, length, genre, album_id) VALUES(
    "Kisapmata",
    259,
    "OPM",
    2
);

INSERT INTO users (email, password) VALUES(
    "user1@mail.com",
    "password"
);

-- Selecting Records 
-- Display all songs

SELECT * FROM songs;

-- Display the title and genre of all songs
SELECT song_name, genre FROM songs;

-- Display song name from
SELECT song_name FROM songs WHERE genre = "OPM"

-- Display the song name and length of OPM songs that are longer than 240 seconds

SELECT song_name, length FROM songs WHERE length > 240 AND genre = "OPM";

-- We can use AND and OR for multiple expressions in the WHERE clause


-- Updating records
-- Updating the length of Kundiman to 2:40
UPDATE songs SET length = 240 WHERE song_name = "Kundiman" -- removing the WHERE clause will update all rows

UPDATE users SET isAdmin = true;

-- DELETING records
-- Delete all OPM songs longer than 2:40
DELETE FROM songs WHERE genre = "OPM" and length > 240;

-- Removing the WHERE clasues